#!/bin/bash

source "$(realpath "${BASH_SOURCE%/*}/env")"

cd "${ORIGINAL_SCRIPT_PATH}" || exit 1
cd ..

git submodule add https://gitlab.com/franferri.io/project_templates/commons/libs.git ./bin/libs

exit 0
